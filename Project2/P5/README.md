5. The GNS3 network should include 3 other virtual machines. One of them in the subnet of Nagios and the others in the other subnet. 
For these virtual machines we suggest the use of the small and specific virtual machines already available at the GNS3 web site (see Linux Core in the Appliances link of the GNS3 web site). 
NRPE should be used to monitor these virtual machines. 
The experience should describe how to use events to implement automatic recovery of services and also how to automatically contact system administrators if automatic recovery fails.
