4. The monitoring of the TODD application should include: 

how to obtain information about the state of java objects; 
how to change the state of java objects; 
how to try to automatically restart the TODD application when it is down; 
how to notify someone when something “happens” in the java application, etc.. 

Basically, the idea is to show how to integrate JMX and Nagios. For that the TODD java application should be used.
